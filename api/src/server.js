const http = require("http");
const { postgraphile } = require("postgraphile");
const PgSimplifyInflectorPlugin = require("@graphile-contrib/pg-simplify-inflector");
const PgManyToManyPlugin = require("@graphile-contrib/pg-many-to-many");
const ConnectionFilterPlugin = require("postgraphile-plugin-connection-filter");

http
  .createServer(
    postgraphile(process.env.DATABASE_URL, "api", {
      appendPlugins: [
        PgSimplifyInflectorPlugin,
        PgManyToManyPlugin,
        ConnectionFilterPlugin
      ],
      watchPg: true,
      graphiql: true,
      enhanceGraphiql: true,
      pgDefaultRole: 'anonymous',
      jwtSecret: process.env.APP_SECRET,
      jwtPgTypeIdentifier: 'api.jwt_token',
      watchPg: true,
    })
  )
  .listen(process.env.PORT);
