import pytest  # noqa
from pprint import pprint
from .fixtures import reset_fixtures


LOGIN_QUERY = '''
    mutation {
      login(input: {email:"$email", password: "$password"}) {
        jwtToken
      }
    }
'''
LOGIN_DATA = {'email': 'peter@example.com', 'password': 'secret'}


@pytest.fixture(autouse=True)
def run_around_tests(app):
    # before test
    reset_fixtures()

    yield

    # after test
    reset_fixtures()


def test_list(gql):
    r = gql(LOGIN_QUERY, LOGIN_DATA)
    result = r.json()
    headers = {
        'Authorization': 'Bearer {}'.format(result['data']['login']['jwtToken'])
    }
    QUERY = '''
{
    users {
        nodes {
            id,
            name,
            email
        }
    }
}
    '''
    r = gql(QUERY, headers=headers)
    result = r.json()
    pprint(result)
