import pytest  # noqa
import json  # noqa
import requests  # noqa
from pprint import pprint  # noqa


LOGIN_QUERY = '''
    mutation {
      login(input: {email:"$email", password: "$password"}) {
        jwtToken
      }
    }
'''
LOGIN_DATA = {'email': 'peter@example.com', 'password': 'secret'}


def test_login(gql):
    r = gql(LOGIN_QUERY, LOGIN_DATA)
    assert r.status_code == 200


def test_me(gql):
    r = gql(LOGIN_QUERY, LOGIN_DATA)
    result = r.json()
    headers = {
        'Authorization': 'Bearer {}'.format(result['data']['login']['jwtToken'])
    }

    QUERY = '''
        query {
          me
        }
    '''
    r = gql(QUERY, headers=headers)
    result = r.json()
    me = json.loads(result['data']['me'])
    assert me['email'] == 'peter@example.com'


def test_refresh_token(gql):
    r = gql(LOGIN_QUERY, LOGIN_DATA)
    data = r.json()
    headers = {
        'Authorization': 'Bearer {}'.format(data['data']['login']['jwtToken'])
    }

    QUERY = '''
        query {
          refreshToken
        }
    '''
    r = gql(QUERY, headers=headers)
    assert r.status_code == 200
