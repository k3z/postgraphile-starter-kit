""" Master CLI commands """
import click  # noqa

from flask import Blueprint


bp = Blueprint('cli', __name__, cli_group='service')


@bp.cli.command()
def hello():
    """ Hello """
    print('hello')


@bp.cli.command()
def fixtures_load():
    from service_app.fixtures import load_fixtures

    load_fixtures()
