import os
from werkzeug.middleware.proxy_fix import ProxyFix
from flask import Flask

from flask_jwt_extended import JWTManager

from service_app.cli.app import bp as cli_app


class AppFactory(Flask):
    def __init__(
        self,
        config='service_app/config.py',
        init_scheduler=False,
        worker=False,
        test_config=False,
    ):
        super(AppFactory, self).__init__('service_app')
        self.wsgi_app = ProxyFix(self.wsgi_app)

        self.config.from_pyfile(os.path.join(os.getcwd(), config))

        if self.config['DEBUG'] is True:
            self.config.update(SEND_FILE_MAX_AGE_DEFAULT=0)

        # Extensions
        jwt = JWTManager(self)

        @jwt.user_loader_callback_loader
        def user_loader_callback(identity):
            class User(object):
                id = 'a561710e-3d73-47ca-9df8-23c09dbba8c6'

            return User()

        # Blueprints
        self.register_blueprint(cli_app)

        @self.route('/')
        def index():
            return ''
