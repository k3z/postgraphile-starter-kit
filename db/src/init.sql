\set QUIET on
\set ON_ERROR_STOP on
set client_min_messages to warning;

-- \ir libs/settings.sql
-- \ir libs/request.sql

\ir api/schema.sql
-- \ir api_private/schema.sql

\ir authorization/roles.sql
\ir authorization/privileges.sql

\ir seed/data.sql
