CREATE TABLE author (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    status VARCHAR(100) NOT NULL DEFAULT 'DISABLED',
    slug VARCHAR(255),
    email VARCHAR(255),
    screenname VARCHAR(255),
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    alias VARCHAR(255),
    entity VARCHAR(255),
    description TEXT,
    quote TEXT,
    social_networks JSON,
    photo VARCHAR(255),
    photo_credits VARCHAR(255),
    attributes JSON,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create index on author (created_at);
create index on author (deleted);
create index on author (status);
create index on author (email);
create index on author (screenname);
