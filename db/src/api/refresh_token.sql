create or replace function refresh_token() RETURNS api.jwt_token AS $$
declare
    usr record;
    token_information api.jwt_token;
begin
    SELECT 'editor', id, name
    INTO token_information
    FROM api."user"
    WHERE id = NULLIF(current_setting('jwt.claims.user_id', TRUE), '')::UUID;
    RETURN token_information::api.jwt_token;
end
$$ stable security definer language plpgsql;
