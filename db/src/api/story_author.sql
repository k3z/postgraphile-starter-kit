create table story_author (
  story_id UUID constraint story_author_story_id_fk references story,
  author_id UUID constraint story_author_author_id_fk references author,
  primary key (story_id, author_id)
);

comment on constraint story_author_story_id_fk on story_author is E'@manyToManyFieldName stories';
comment on constraint story_author_author_id_fk on story_author is E'@manyToManyFieldName authors';


create function "story_authorsPublic"(s api.story)
returns setof api.author as $$
  select api.author.*
  from api.author
  inner join api.story_author
  on (api.story_author.author_id = api.author.id)
  where api.story_author.story_id = s.id
  and api.author.status = 'ENABLED';
$$ language sql stable;
