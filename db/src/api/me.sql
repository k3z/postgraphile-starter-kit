create or replace function me() returns json as $$
declare
    usr record;
begin
    select * from api."user"
    where id = NULLIF(current_setting('jwt.claims.user_id', TRUE), '')::UUID
    into usr;

    if usr is null then
        raise exception 'user not found';
    else
        return json_build_object(
            'id', usr.id,
            'name', usr.name,
            'email', usr.email
        );
    end if;
end
$$ stable security definer language plpgsql;
