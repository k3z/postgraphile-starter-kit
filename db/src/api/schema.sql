drop schema if exists api cascade;
create schema api;
set search_path = api, public;

\ir user.sql
\ir login.sql
\ir me.sql
\ir refresh_token.sql
\ir post.sql
\ir story.sql
\ir author.sql
\ir story_author.sql
