CREATE TABLE "user" (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    name text NOT NULL,
    email text NOT NULL UNIQUE,
    password text NOT NULL
);

create or replace function encrypt_pass() returns trigger as $$
begin
  if new.password is not null then
    new.password = public.crypt(new.password, public.gen_salt('bf'));
  end if;
  return new;
end
$$ language plpgsql;

create trigger user_encrypt_pass_trigger
before insert or update on "user"
for each row
execute procedure encrypt_pass();


CREATE TYPE api.jwt_token AS (
  role TEXT,
  user_id uuid,
  name TEXT
);
