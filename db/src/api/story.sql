CREATE TABLE story (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    status VARCHAR(100) NOT NULL DEFAULT 'DISABLED',
    scope VARCHAR(100) NOT NULL DEFAULT 'PUBLIC',
    scheme VARCHAR(100) NOT NULL DEFAULT 'STORY',
    content_type VARCHAR(100),
    comments_allowed BOOLEAN NOT NULL DEFAULT FALSE,
    slug VARCHAR(255),
    title TEXT,
    headline TEXT,
    extract TEXT,
    content TEXT,
    notes TEXT,
    cover_image VARCHAR(255),
    cover_image_credits VARCHAR(255),
    attributes  JSON,
    published_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create index on story (created_at);
create index on story (deleted);
create index on story (status);
create index on story (scope);
create index on story (scheme);
