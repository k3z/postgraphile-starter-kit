CREATE or replace FUNCTION login(email TEXT, password TEXT) RETURNS api.jwt_token AS $$
DECLARE
    token_information api.jwt_token;
BEGIN
    SELECT 'editor', id, name
           INTO token_information
           FROM api."user"
           WHERE api."user".email = $1
                 AND api."user".password = crypt($2, api."user".password);
    RETURN token_information::api.jwt_token;
end;
$$ LANGUAGE PLPGSQL VOLATILE STRICT SECURITY DEFINER;
