CREATE TABLE post (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    title text NOT NULL,
    tags json NOT NULL,
    content text NOT NULL,
    published BOOLEAN NOT NULL DEFAULT FALSE,
    author_id uuid NOT NULL REFERENCES "user" (id) ON DELETE CASCADE
);
