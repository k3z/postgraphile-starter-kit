INSERT INTO api.user (id, name, email, password)
VALUES ('fc2f1e88-04b8-47ee-a11b-2d6f8b14f22e', 'Peter',
        'peter@example.com',
        'secret');


INSERT INTO api.user (id, name, email, password)
VALUES ('243d345e-498f-4f9e-8e5f-c78b7ca74656', 'Kike',
        'mike@example.com',
        'secret');


INSERT INTO api.user (id, name, email, password)
VALUES ('90a9957b-3580-44a6-895e-0b954b2db81f', 'Maria',
        'maria@example.com',
        'secret');


INSERT INTO api.post (author_id, title, content, tags, published)
VALUES ('fc2f1e88-04b8-47ee-a11b-2d6f8b14f22e',
        'The truth about All',
        'A lot of lines',
        '{"foo": "bar"}',
        TRUE);


INSERT INTO api.post (author_id, title, content, tags, published)
VALUES ('243d345e-498f-4f9e-8e5f-c78b7ca74656',
        '7 Things about me',
        'Too much content',
        '{"foo": "bar"}',
        TRUE);


INSERT INTO api.post (author_id, title, content, tags, published)
VALUES ('243d345e-498f-4f9e-8e5f-c78b7ca74656',
        'Peter is the best',
        'my draft',
        '{"foo": "bar"}',
        FALSE);


INSERT INTO api.post (author_id, title, content, tags, published)
VALUES ('90a9957b-3580-44a6-895e-0b954b2db81f',
        'I bought new shoes and new shirt',
        'more than 100 lines',
        '{"foo": "bar"}',
        TRUE);

