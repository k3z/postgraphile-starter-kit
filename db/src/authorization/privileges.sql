grant usage on schema api to anonymous, editor;

revoke all privileges on function api.login(text, text) from public;
GRANT EXECUTE ON FUNCTION api.login(email TEXT, password TEXT) TO anonymous;
GRANT EXECUTE ON FUNCTION api.login(email TEXT, password TEXT) TO editor;

revoke all privileges on function api.me() from public;
GRANT EXECUTE ON FUNCTION api.me() TO anonymous;
GRANT EXECUTE ON FUNCTION api.me() TO editor;

revoke all privileges on function api.refresh_token() from public;
GRANT EXECUTE ON FUNCTION api.refresh_token() TO anonymous;
GRANT EXECUTE ON FUNCTION api.refresh_token() TO editor;


GRANT SELECT ON api.post TO anonymous;
GRANT SELECT(id) ON api."user" TO anonymous;
GRANT SELECT(name) ON api."user" TO anonymous;
GRANT SELECT, INSERT, UPDATE, DELETE ON api."user" TO editor;
GRANT SELECT, INSERT, UPDATE, DELETE ON api."post" TO editor;
GRANT SELECT, INSERT, UPDATE, DELETE ON api."author" TO editor;
GRANT SELECT, INSERT, UPDATE, DELETE ON api."story" TO editor;
GRANT SELECT, INSERT, UPDATE, DELETE ON api."story_author" TO editor;
