drop role if exists anonymous;
create role anonymous;
grant anonymous to admin;

drop role if exists editor;
create role editor;
grant editor to admin;
