# Postgraphile Starter Kit

This Project Helps me to understand Postgraphile fundamentals, and helps me to start future projects with ready to use codebase.

## Prerequisites

[TODO]

## Try it

	$ make init

Access GraphQLi http://0.0.0.0:5433/graphiql


### Reset fixtures

	$ make install-db
	$ dcexec app flask service fixtures-load


### Login

	mutation {
	  signin(input: {email:"peter@example.com", password: "secret"}) {
	    jwtToken
	  }
	}

This call return a valid JWT Token


	{
	  "data": {
	    "signin": {
	      "jwtToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIj…"
	    }
	  }
	}


With current payload


	{
	  "role": "editor",
	  "user_id": "fc2f1e88-04b8-47ee-a11b-2d6f8b14f22e",
	  "name": "Peter",
	  "iat": 1606463845,
	  "exp": 1606550245,
	  "aud": "postgraphile",
	  "iss": "postgraphile"
	}

### Refresh database

If you make changes in `db/src/`, you need to refresh data in DB

	$ make install-db
