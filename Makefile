NOW = $(sh date +%Y%m)

default: here


here:
	@sh -c "pwd"
	@echo "---"
	@echo "Access GraphQLi http://0.0.0.0:5433/graphiql"
	@echo "---"

init: install install-db

install:
	@docker-compose pull
	@docker-compose build
	@docker-compose up -d

install-db:
	@docker exec service-db sh -c "rm -rf /tmp/sql/"
	@docker cp db/src/ service-db:/tmp/sql/
	@docker-compose exec db sh -c "cd /tmp/sql/ && psql --quiet -U \${POSTGRES_USER} \${POSTGRES_DB} -f /tmp/sql/init.sql"
	@docker-compose exec app flask service fixtures-load
